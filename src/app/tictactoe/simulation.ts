import {newArray} from '@angular/compiler/src/util';
import {Move} from './move';

export class Simulation {
  public board;

  public availableMoves;

  private rowBuffer;
  private columnBuffer;
  private diagBuffer;
  private antiDiagBuffer;

  constructor(board, availableMoves: Move[]) {
    this.board = board;
    this.initLineBuffers();
    this.availableMoves = availableMoves;
  }

  // Initialise all line buffers
  initLineBuffers(): void {
    this.rowBuffer = newArray(this.board.length);
    this.columnBuffer = newArray(this.board.length);
    this.diagBuffer = newArray(this.board.length);
    this.antiDiagBuffer = newArray(this.board.length);
  }

  // Plays 'X' or 'O' depending on the parity of this.turnNumber
  updateBoard(move: Move, symbol: string): void {
    if (this.board[move.row][move.column] === ' ') {
      this.board[move.row][move.column] = symbol;
    } else {
      console.log('BOARD NOT UPDATED');
    }
  }

  // Update availableMoves
  updateAvailableMoves(move: Move): void {
    console.log('AVAILABLE MOVES LENGTH ', this.availableMoves.length, this.availableMoves);
    const moveIndex = this.availableMoves.findIndex(m => m.row === move.row && m.column === move.column);
    if (moveIndex > -1) {
      this.availableMoves.splice(moveIndex, 1);
    } else {
      console.log('MOVE NOT FOUND');
    }
  }

  // Tells if game is over
  isThisGameOver(): boolean {
    return this.availableMoves.length === 0;
  }

  // Checks who is winning if a line is found
  checkWinner(): string {
    const allEqualX = arr => arr.every(v => v === 'X');
    const allEqualO = arr => arr.every(v => v === 'O');

    for (let i = 0; i < this.board.length; i++) {
      for (let j = 0; j < this.board.length; j++) {
        this.rowBuffer[j] = this.board[i][j];
        this.columnBuffer[j] = this.board[j][i];
        if (i === j) {
          this.diagBuffer[i] = this.board[i][j];
          this.antiDiagBuffer[i] = this.board[this.board.length - 1 - i][j];
        }
      }

      // Checks if there is a rowLine
      if (allEqualX(this.rowBuffer) || allEqualX(this.columnBuffer) || allEqualX(this.diagBuffer) || allEqualX(this.antiDiagBuffer)) {
        return 'X';
      }
      if (allEqualO(this.rowBuffer)  || allEqualO(this.columnBuffer) || allEqualO(this.diagBuffer) || allEqualO(this.antiDiagBuffer)) {
        return 'O';
      }
    }
    if (this.isThisGameOver()) {
      return 'tie';
    }
    return null;
  }

  // Checks if there is a line in the board, either it is a diagonal, a row or a column
  isThereALine(): boolean {
    const allEqualX = arr => arr.every( v => v === 'X' );
    const allEqualO = arr => arr.every( v => v === 'O' );

    for (let i = 0; i < this.board.length; i++) {
      for (let j = 0; j < this.board.length; j++) {
        this.rowBuffer[j] = this.board[i][j];
        this.columnBuffer[j] = this.board[j][i];
        if (i === j) {
          this.diagBuffer[i] = this.board[i][j];
          this.antiDiagBuffer[i] = this.board[this.board.length - 1 - i][j];
        }
      }
      // Checks if there is a rowLine
      if (allEqualX(this.rowBuffer) || allEqualO(this.rowBuffer)) {
        console.log(this.rowBuffer, 'A row was found');
        return true;
      }

      // Checks if there is a columnLine
      if (allEqualX(this.columnBuffer) || allEqualO(this.columnBuffer)) {
        console.log(this.columnBuffer, 'A column was found');
        return true;
      }

      // Checks if there is a diagLine like \
      if (allEqualX(this.diagBuffer) || allEqualO(this.diagBuffer)) {
        console.log(this.diagBuffer, 'A diagonal was found');
        return true;
      }

      // Checks if there is a antiDiagLine like /
      if (allEqualX(this.antiDiagBuffer) || allEqualO(this.antiDiagBuffer)) {
        console.log(this.antiDiagBuffer, 'An anti diagonal was found');
        return true;
      }
      // console.log(this.rowBuffer, 'LINE ', i);
      // console.log(this.columnBuffer, 'COLUMN ', i);
    }

    // console.log(this.diagBuffer, 'DIAG');
    // console.log(this.antiDiagBuffer, 'ANTI DIAG');
    return false;
  }
}
