import {Player} from './player';
import {Move} from './move';
import {Simulation} from './simulation';

export class AI extends Player {

  private readonly strategy: string;
  private simulation: Simulation;
  private readonly opponent: string;

  private readonly scores = {
    X: 10,
    O: -10,
    tie: 0
  };

  constructor(name: string, symbol: string, strategy: string) {
    super(name, symbol);
    this.strategy = strategy;
    this.type = 'AI';
    this.opponent = this.getSymbol() === 'X' ? 'O' : 'X';
  }

  autoPlay(board, availableMoves: Move[]): Move {
    if (this.strategy === '0') {
      return this.idiot(availableMoves);
    } else if (this.strategy === '1') {
      return this.heuristic(board, availableMoves);
    } else if (this.strategy === '2') {
      const boardCopy = JSON.parse(JSON.stringify(board));
      const availableMovesCopy = JSON.parse(JSON.stringify(availableMoves));
      return this.minimax(boardCopy, availableMovesCopy);
    }
    return new Move([0, 0]);
  }

  // Idiot algorithm
  idiot(availableMoves: Move[]): Move {
    const randomMoveIndex = this.getRandomInt(availableMoves.length);
    return availableMoves[randomMoveIndex];
  }

  // Heuristic algorithm
  heuristic(board, availableMoves): Move {
    for (const move of availableMoves) {
      const boardCopy = JSON.parse(JSON.stringify(board));
      this.simulation = new Simulation(boardCopy, availableMoves);
      this.simulation.updateBoard(move, this.getSymbol());
      if (this.simulation.isThereALine()) {
        return move;
      }
    }

    for (const move of availableMoves) {
      const copy = JSON.parse(JSON.stringify(board));
      this.simulation = new Simulation(copy, availableMoves);
      this.simulation.updateBoard(move, this.getSymbol() === 'X' ? 'O' : 'X');
      if (this.simulation.isThereALine()) {
        return move;
      }
    }
    return this.idiot(availableMoves);
  }

  // Minimax algorithm
  minimax(board, availableMoves): Move {
    this.simulation = new Simulation(board, availableMoves);

    let bestScore = -Infinity;
    let finalMove: Move;
    let i = 0;

    for (const move of availableMoves) {
      // We play the first possible move among the availableMovesCopy
      this.simulation.updateBoard(move, this.getSymbol());
      this.simulation.updateAvailableMoves(move);
      const score = this.miniMaxComputing(0, false);
      // Once we get the current move score, we store it if it's better then we erase our play
      console.log(i++, ' iiiiiiiiiiiiiii');
      this.simulation.updateBoard(move, ' ');
      // We put back the move in availableMoves
      this.simulation.availableMoves.push(move);
      if (score > bestScore) {
        console.log(score, 'The move score is : ', score, ' for the move ', move.row, move.column);
        bestScore = score;
        finalMove = new Move([move.row, move.column]);
      }
    }
    return finalMove;
  }

  // Minimax computing
  miniMaxComputing(depth: number, isMax: boolean): number {

    // We check if there is a winner
    const result = this.simulation.checkWinner();
    if (result !== null) {
      return this.scores[result];
    }

    if (isMax) {
      let bestScore = -Infinity;
      for (const move of this.simulation.availableMoves) {
        let j = 0;
        console.log(j++, ' jjjjjjjjjjjjjjjjjjj');
        // We play the first possible move among the availableMovesCopy
        this.simulation.updateBoard(move, this.getSymbol());
        this.simulation.updateAvailableMoves(move);
        console.log('Max is playing', this.getSymbol(), ' in move ', move);
        const score = this.miniMaxComputing(depth + 1, false);
        // Once again we get the current move score, we store it if it's better then we erase our play
        this.simulation.updateBoard(move, ' ');
        // We put back the move in availableMoves
        this.simulation.availableMoves.push(move);
        bestScore = Math.max(score, bestScore);
      }
      return bestScore;
    } else {
      let bestScore = Infinity;
      for (const move of this.simulation.availableMoves) {
        // We play the first possible move among the availableMovesCopy
        this.simulation.updateBoard(move, this.opponent);
        this.simulation.updateAvailableMoves(move);
        console.log('Min is playing', this.opponent, ' in move ', move);
        const score = this.miniMaxComputing(depth + 1, true);
        // Once again we get the current move score, we store it if it's better then we erase our play
        this.simulation.updateBoard(move, ' ');
        // We put back the move in availableMoves
        this.simulation.availableMoves.push(move);
        bestScore = Math.min(score, bestScore);
      }
      console.log(this.simulation.board, this.simulation.availableMoves);
      console.log(depth, ' depth');
      console.log(bestScore, ' bestscore');
      return bestScore;
    }
  }
}
