import {Move} from './move';

export class Player {
  public name;
  private symbol;
  protected type;

  constructor(name: string, symbol: string) {
    this.name = name;
    this.symbol = symbol;
    this.type = 'Human';
  }

  // Sets the player symbol
  setSymbol(symbol: string): void {
    this.symbol = symbol;
  }

  // Gets the player symbol
  getSymbol(): string {
    return this.symbol;
  }

  // Returns if the player is an AI or not
  isAI(): boolean {
    return this.type === 'AI';
  }

  // Gives a random int amongs all int from 0 to max
  getRandomInt(max: number): number {
    return Math.floor(Math.random() * Math.floor(max));
  }
}
