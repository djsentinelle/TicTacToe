
export class Move {

  public row;
  public column;
  public symbol: string;

  constructor(move: number[]) {
    this.row = move[0];
    this.column = move[1];
  }
}
