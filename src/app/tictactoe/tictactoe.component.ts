import { Component, OnInit } from '@angular/core';
import { Game } from './game';
import {AI} from './AI';
import {Player} from './player';
import {Move} from './move';

@Component({
  selector: 'app-tictactoe',
  templateUrl: './tictactoe.component.html',
  styleUrls: ['./tictactoe.component.scss']
})
export class TictactoeComponent implements OnInit {

  constructor() { }

  private HTMLBoard;
  private HTMLBoardContainer;
  private squareContainer;
  private nav;
  private form;

  public game: Game;

  ngOnInit(): void {
    this.game = new Game();
    this.form = document.querySelector('form') as HTMLFormElement;
    this.form.addEventListener('submit', () => (this.handleForm(event)));
    this.HTMLBoard =  document.querySelector('.boardContainer') as HTMLTableElement;
    this.HTMLBoardContainer = document.querySelector('.square') as HTMLElement;
    this.squareContainer = document.querySelector('.squareContainer') as HTMLElement;
    this.nav = document.querySelector('nav') as HTMLElement;
  }

  // Retrieves the radio button values for players
  setGame(): void {
    const formInputs = this.form.querySelectorAll('input');
    const formSelects = this.form.querySelectorAll('select');

    let nameAI = '';
    const strategyAI1 = formSelects[0].value;

    // Player 1
    if (formInputs[0].checked) {
      this.game.player1 = new Player(formInputs[0].value + ' 1', '');
    } else {
      if (formSelects[0].value === '0') {
        nameAI = 'Idiot';
      } else if (formSelects[0].value === '1') {
        nameAI = 'Heuristic';
      } else if (formSelects[0].value === '2'){
        nameAI = 'Minimax';
      }
      this.game.player1 = new AI(nameAI + ' 1', '', strategyAI1);
    }
    console.log('We set player 1 as : name', this.game.player1.name, ' with the type', this.game.player1.isAI());

    const strategyAI2 = formSelects[1].value;
    // Player 2
    if (formInputs[2].checked) {
      this.game.player2 = new Player(formInputs[2].value + ' 2', '');
    } else {
      if (formSelects[1].value === '0') {
        nameAI = 'Idiot';
      } else if (formSelects[1].value === '1') {
        nameAI = 'Heuristic';
      } else if (formSelects[1].value === '2') {
        nameAI = 'Minimax';
      }
      this.game.player2 = new AI(nameAI + ' 2', '', strategyAI2);
    }
    console.log('We set player 2 as : name', this.game.player2.name, ' with the type', this.game.player2.isAI());

    // First player
    if (formInputs[4].checked) {
      this.game.player1.setSymbol('X');
      this.game.player2.setSymbol('O');
      this.game.whoIsPlaying = this.game.player1;
    } else if (formInputs[5].checked) {
      this.game.player2.setSymbol('X');
      this.game.player1.setSymbol('O');
      this.game.whoIsPlaying = this.game.player2;
    } else {
      if (this.getRandomInt(2) !== 0) {
        this.game.player1.setSymbol('X');
        this.game.player2.setSymbol('O');
        this.game.whoIsPlaying = this.game.player1;
      } else {
        this.game.player2.setSymbol('X');
        this.game.player1.setSymbol('O');
        this.game.whoIsPlaying = this.game.player2;
      }
    }
    console.log('First player is ', this.game.whoIsPlaying);

    // Board Length
    if (formInputs[7].validity) {
      const textArea = formInputs[7];
      this.game.setBoardLength(parseInt(textArea.value, 10));
    }

    this.game.gameType = (this.game.player1.isAI() && this.game.player2.isAI()) ? 'Observe' : 'In Game';
    this.game.initGame();
    this.initHTMLBoard();
    if (this.game.whoIsPlaying.isAI()) {
      this.executeMove();
    }
  }

  // Initialize the associated HTML board by creating his cells
  initHTMLBoard(): void {
    for (let i = 0; i < this.game.boardLength; i++) {
      const currentRow = this.HTMLBoard.insertRow();

      for (let j = 0; j < this.game.boardLength; j++) {
        const currentCell = currentRow.insertCell();

        currentCell.style.height = `calc(100% / ${this.game.boardLength})`;
        currentCell.innerText = ' ';

        this.addListenerPlayOnCell(i, j, currentCell); // Add listener onclick on each cell
      }
    }
    this.displayHTMLBoard();
  }

  // Update HTMLBoard after a move
  updateHTMLBoard(move: Move): void {
    this.HTMLBoard.rows[move.row].cells[move.column].innerText = this.game.board[move.row][move.column];
  }

  executeMove = (i?: number, j?: number) => {
    const move = this.game.play(i, j);
    console.log('THE MOVE BEFORE IT IS ADD ON HTML', move, ' ', this.game.whoIsPlaying);
    if (!move) {
      return;
    }
    this.updateHTMLBoard(move);
    this.game.isOver = this.game.isThisGameOver();

    if (this.game.isOver) {
      this.game.displayWinner();
      this.displayGameOverBoard();
      return;
    }
    this.game.setWhoIsPlaying();
    if (this.game.whoIsPlaying.isAI()) {
      setTimeout(this.executeMove, 500);
    }
  }

  // Sets onclick listeners on each available case
  addListenerPlayOnCell(i: number, j: number, currentCell: HTMLTableDataCellElement): void {
    currentCell.addEventListener('click', () => {
      if (!this.game.whoIsPlaying.isAI()) {
        this.executeMove(i, j);
      }
    });
  }

  // Erase this.board, this.HTMLBoard and this.boardLength
  eraseHTMLBoard(): void {
    location.href = '/';
  }

  // Clear this.board, this.HTMLBoard
  clearHTMLBoard(): void {
    if (this.game.isOver) { } {
      this.hideHTMLBoard();
    }
    this.HTMLBoard.innerHTML = '';
    this.ngOnInit();
  }

  // Shows the board and hide the nav
  displayHTMLBoard(): void {
    this.HTMLBoard.style.fontSize = `calc(3000% / ${this.game.boardLength})`; // Sets the font-size property depending on the boardLength

    const gridTemplatePropertyValue = // Prepare the right css grid template depending on the boardLength
      'repeat('
      + this.game.boardLength
      + ', 1fr) / repeat('
      + this.game.boardLength
      + ', 1fr)'
    ;

    this.HTMLBoard.style.setProperty('grid-template', gridTemplatePropertyValue); // Sets the template after all elements are created
    this.squareContainer.setAttribute('style', 'display: flex;');
    this.nav.setAttribute('style', 'display: none;');
    this.HTMLBoardContainer.setAttribute('style', 'padding-top: 100%;');

    const beforeGameItems = document.querySelectorAll('.beforeGame');
    beforeGameItems.forEach(item => {
      item.setAttribute('style', 'display: none;');
    });

    const inGameItems = document.querySelectorAll('.inGame');
    inGameItems.forEach(item => {
      item.setAttribute('style', 'display: flex;');
    });
  }

  // Sets gameOver HTML Template
  displayGameOverBoard(): void {
    const inGameItems = document.querySelectorAll('.inGame');
    inGameItems.forEach(item => {
      item.setAttribute('style', 'display: none;');
    });

    const gameOverItems = document.querySelectorAll('.gameOver');
    gameOverItems.forEach(item => {
      item.setAttribute('style', 'display: flex;');
    });
  }

  // Shows the nav and hide the board
  hideHTMLBoard(): void {
    this.nav.setAttribute('style', 'display: flex;');
    this.HTMLBoardContainer.setAttribute('style', 'display: none;');
    this.HTMLBoardContainer.setAttribute('style', 'padding-top: 0%;');

    const beforeGameItems = document.querySelectorAll('.beforeGame');
    beforeGameItems.forEach(item => {
      item.setAttribute('style', 'display: flex;');
    });

    const inGameItems = document.querySelectorAll('.inGame');
    inGameItems.forEach(item => {
      item.setAttribute('style', 'display: none;');
    });

    const gameOverItems = document.querySelectorAll('.gameOver');
    gameOverItems.forEach(item => {
      item.setAttribute('style', 'display: none;');
    });
  }

  // Block reload page on submit form
  handleForm(event: Event): void {
    event.preventDefault();
  }

  // Gives a random int amongs all int from 0 to max
  getRandomInt(max: number): number {
    return Math.floor(Math.random() * Math.floor(max));
  }

}
