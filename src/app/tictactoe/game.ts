import {Player} from './player';
import {newArray} from '@angular/compiler/src/util';
import {Move} from './move';

export class Game {
  public boardLength: number;
  public board;

  public turnNumber: number;
  public whoIsPlaying;
  public winner: Player;
  public gameType: string;
  public firstPlayer: Player;
  public availableMoves;
  public isOver: boolean;
  private hasLine: boolean;

  private rowBuffer;
  private columnBuffer;
  private diagBuffer;
  private antiDiagBuffer;

  public player1: Player;
  public player2: Player;
  private readonly emptyPlayer = new Player(' ', ' ');

  constructor() {
    this.turnNumber = 0;
    this.isOver = false;
    this.hasLine = false;

    this.player1 = this.emptyPlayer;
    this.player2 = this.emptyPlayer;
    this.whoIsPlaying = this.emptyPlayer;
    this.winner = this.emptyPlayer;
  }

  // Sets the boarLength with the given parameter
  setBoardLength(length: number): void {
    this.boardLength = length;
  }

  // Init the game
  initGame(): void {
    this.initLineBuffers();
    this.initBoard();
  }

  // Initialise all line buffers
  initLineBuffers(): void {
    this.rowBuffer = newArray(this.boardLength);
    this.columnBuffer = newArray(this.boardLength);
    this.diagBuffer = newArray(this.boardLength);
    this.antiDiagBuffer = newArray(this.boardLength);
  }

  // Initialize the board with empty strings
  initBoard(): void {
    this.availableMoves = [];
    this.board = newArray(this.boardLength);
    for (let i = 0; i < this.boardLength; i++) {
      this.board[i] = newArray(this.boardLength);
      for (let j = 0; j < this.boardLength; j++) {
        this.board[i][j] = ' ';
        this.availableMoves.push(new Move([i, j]));
      }
    }
    // console.log('Available moves after init', this.availableMoves);
  }

  // Plays 'X' or 'O' depending on the parity of this.turnNumber
  updateBoard(i: number, j: number): boolean {
    // console.log(this.board[i][j], this.board[i][j] === ' ');
    if (this.board[i][j] === ' ') {
      if (this.turnNumber % 2 === 0) {
        this.board[i][j] = 'X';
      } else {
        this.board[i][j] = 'O';
      }
      return true;
    } else {
      console.log('THIS CASE IS ALREADY FILLED');
      return false;
    }
  }

  // Update availableMoves
  updateAvailableMoves(move: Move): void {
    const moveIndex = this.availableMoves.findIndex(m => m.row === move.row && m.column === move.column);
    if (moveIndex > -1) {
      this.availableMoves.splice(moveIndex, 1);
    }
  }

  // Play and end turn
  play(i: number, j: number): Move {
    console.log(this.whoIsPlaying.isAI());
    const move = this.whoIsPlaying.isAI() ? this.whoIsPlaying.autoPlay(this.board, this.availableMoves) : new Move([i, j]);
    if (!this.updateBoard(move.row, move.column)) {
      return null;
    }
    console.log('MOVE ', move, ' BY', this.whoIsPlaying);
    // console.log('MOVES BEFORE FILTERING', this.availableMoves);
    this.updateAvailableMoves(move);
    console.log('FILTERED MOVES ', this.availableMoves);
    this.turnNumber++;
    console.log('REAL BOARD ', this.board);

    console.log('______________________________________');
    return move;
  }

  // Tells if game is over
  isThisGameOver(): boolean {
    return this.turnNumber === this.boardLength * this.boardLength || this.isThereALine();
  }

  // Checks if there is a line in the board, either it is a diagonal, a row or a column
  isThereALine(): boolean {
    const allEqualX = arr => arr.every( v => v === 'X' );
    const allEqualO = arr => arr.every( v => v === 'O' );

    for (let i = 0; i < this.boardLength; i++) {
      for (let j = 0; j < this.boardLength; j++) {
        this.rowBuffer[j] = this.board[i][j];
        this.columnBuffer[j] = this.board[j][i];
        if (i === j) {
          this.diagBuffer[i] = this.board[i][j];
          this.antiDiagBuffer[i] = this.board[this.boardLength - 1 - i][j];
        }
      }
      // Checks if there is a rowLine
      if (allEqualX(this.rowBuffer) || allEqualO(this.rowBuffer)) {
        this.hasLine = true;
        console.log(this.rowBuffer, 'A row was found');
        return this.hasLine;
      }

      // Checks if there is a columnLine
      if (allEqualX(this.columnBuffer) || allEqualO(this.columnBuffer)) {
        this.hasLine = true;
        console.log(this.columnBuffer, 'A column was found');
        return this.hasLine;
      }

      // Checks if there is a diagLine like \
      if (allEqualX(this.diagBuffer) || allEqualO(this.diagBuffer)) {
        this.hasLine = true;
        console.log(this.diagBuffer, 'A diagonal was found');
        return this.hasLine;
      }

      // Checks if there is a antiDiagLine like /
      if (allEqualX(this.antiDiagBuffer) || allEqualO(this.antiDiagBuffer)) {
        this.hasLine = true;
        console.log(this.antiDiagBuffer, 'An anti diagonal was found');
        return this.hasLine;
      }

      // console.log(this.rowBuffer, 'LINE ', i);
      // console.log(this.columnBuffer, 'COLUMN ', i);
    }

    // console.log(this.diagBuffer, 'DIAG');
    // console.log(this.antiDiagBuffer, 'ANTI DIAG');
    console.log('NO LINE');

    return this.hasLine;
  }

  // Display the winner
  displayWinner(): void {
    if (this.isThereALine()) {
      this.winner = this.whoIsPlaying;
    } else {
      this.winner.name = 'Nobody';
    }
  }

  // Display the winner
  setWhoIsPlaying(): void {
    if (this.whoIsPlaying.name === this.player1.name) {
      this.whoIsPlaying = this.player2;
    } else {
      this.whoIsPlaying = this.player1;
    }
  }
}
